<?php

namespace App\Http\Controllers\Admin;

use App\Models\Client;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::paginate(20);
        return view('admin.clients.index', compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.clients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'edrpou' => 'required|integer',
            'name' => 'required|string',
            'fullname' => 'required|string',
            'adress' => 'required|string',
            'legal_adress' => 'required|string'
        ]);

        $client = Client::create([
            'user_id' => auth()->user()->id,
            'edrpou' => $request->edrpou,
            'name' => $request->name,
            'fullname' => $request->fullname,
            'adress' => $request->adress,
            'legal_adress' => $request->legal_adress
        ]);

        if ($client) {
            return redirect('/admin/clients/');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        return view('admin.clients.show',compact('client'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        return view('admin.clients.edit', compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        $request->validate([
            'edrpou' => 'required|integer',
            'name' => 'required|string',
            'fullname' => 'required|string',
            'adress' => 'required|string',
            'legal_adress' => 'required|string',
        ]);

        $client->edrpou = $request->edrpou;
        $client->name = $request->name;
        $client->fullname = $request->fullname;
        $client->adress = $request->adress;
        $client->legal_adress = $request->legal_adress;

        if ($client->save()) {
            Session::flash('success', 'Client update');
            return redirect('/admin/clients/');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        if (Client::destroy($client->id)) {
            Session::flash('error', 'Client '.$client->name.' has been deleted');
            return redirect('/admin/clients/');
        };
    }
}
