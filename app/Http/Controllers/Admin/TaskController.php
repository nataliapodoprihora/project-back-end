<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Models\Order;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class TaskController extends Controller
{

    public function index()
    {
        $tasks = Task::where('user_id', '=', auth()->user()->id)->paginate(20);
        return view('admin.tasks.index', compact('tasks'));
    }

    public function edit(Task $task)
    {
        $clients = Client::all();
        $orders = Order::all();
        return view('admin.tasks.edit', compact('task', 'clients', 'orders'));
    }

    public function create(Request $request)
    {
        $clients = Client::all();
        $orders = Order::all();
        return view('admin.tasks.create', compact('orders', 'clients'));
    }

    public function update(Request $request, Task $task)
    {
        $request->validate([
            'name' => 'required|string',
            'description' => 'required|string',
            'client' => 'required|integer',
            'order' => 'required|integer',
            'duration' => 'required|integer'
        ]);

        $task->name = $request->name;
        $task->description = $request->description;
        $task->client_id = $request->client;
        $task->order_id = $request->order;
        $task->duration = $request->duration;
        $task->approved = 0;

        if ($task->save()) {
            Session::flash('success', 'Task update');
            return redirect('/admin/tasks');
        }
    }

    public function show(Task $task)
    {
        $client = Client::where('id', '=', $task->client->id);
        return view('admin.tasks.show', compact('task', 'client'));
    }

    public function destroy(Task $task)
    {
        if (Task::destroy($task->id)) {
            Session::flash('error', 'Task ' . $task->name . ' has been deleted');
            return redirect()->back();
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'description' => 'required|string',
            'duration' => 'required|integer',
            'client' => 'required|integer',
            'order' => 'required|integer'
        ]);

        $task = Task::create([
            'name' => $request->name,
            'description' => $request->description,
            'duration' => $request->duration,
            'user_id' => auth()->user()->id,
            'client_id' => $request->client,
            'order_id' => $request->order,
            'approved' => '0',
            'closed' => '0'
        ]);

        if ($task) {
            return redirect('/admin/tasks');
        }
    }

    public function changePublishedStatus(Task $task){
        $task->approved == '1' ? $task->approved = '0' : $task->approved = '1';
        if($task->save()){
            return redirect()->back();
        }
    }

    public function changeClosed(Task $task){
        $task->closed == '1' ? $task->closed = '0' : $task->closed = '1';
        if($task->save()){
            return redirect()->back();
        }
    }
}
