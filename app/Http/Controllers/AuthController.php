<?php

namespace App\Http\Controllers;

use App\Models\InviteUser;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function showLogin()
    {
//        $referer = app('Illuminate\Routing\UrlGenerator')->previous();
//        $redirectTo = strstr($referer, 'invite') ? $referer :'/dashboard';
        if (Auth::check()) {
            return redirect('/');
        } else {
            return view('auth');//, ['redirect_url' =>$redirectTo]);
        }
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|string'
        ]);

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            return response()->json(['id' => auth()->user()->id], 200);
        }

        return response()->json(['errors' => ['email' => ['There is no user with this E-mail address or password!']]], 419);
    }

    public function register(Request $request)
    {
//        $invite = InviteUser::where('email', '=', $request->email)->first();
//        if ($invite) {
//            $role = 'user';
//            InviteUser::destroy($invite->id);
//        } else {
//            $role = 'client';
//        }

        $request->validate([
            'email' => 'required|email|unique:users',
            'firstname' => 'required|string',
            'password' => 'required|string|confirmed'
        ]);

        $user = User::create([
            'email' => $request->email,
            'password' => bcrypt($request->password),
//            'role' => $role,
            'role' => env("DEFAULT_ROLE"),
        ]);

        if ($user) {
            $profile = Profile::create([
                'user_id' => $user->id,
                'firstname' => $request->firstname,
                'lastname' => $request->lastname
            ]);
            if ($profile) {
                return $this->login($request);
            } else {
                if (User::destroy[$user->id]) {
                    return response()->json(['errors' => ['general' => ['Something wrong! Try again later!']]], 411);
                }
            }
        }
        return response()->json(['errors' => ['general' => ['Something wrong! Try again later!']]], 411);
    }
}
