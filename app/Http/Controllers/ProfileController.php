<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index(){
        return view('profile');
    }

    public function update(Request $request, Profile $profile){
        $request->validate([
            'firstname'=>'nullable|string',
            'lastname'=>'nullable|string',
            'country'=>'nullable|string',
            'city'=>'nullable|string',
            'adress'=>'nullable|string',
            'phone'=>'nullable|string',
            'sex'=>'nullable|string',
        ]);

        $user = auth()->user()->profile;

        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->country = $request->country;
        $user->city = $request->city;
        $user->adress = $request->adress;
        $user->phone = $request->phone;
        $user->sex = $request->sex;

        if($user->save()){
            return redirect()->back();
        }
    }

}
