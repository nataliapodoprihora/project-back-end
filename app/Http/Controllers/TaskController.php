<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\History;
use App\Models\Order;
use App\Models\Task;
use GoogleMaps\GoogleMaps;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Stevebauman\Location\Drivers\IpInfo;
use Stevebauman\Location\Location;


class TaskController extends Controller
{

    public function index()
    {
        $tasks = Task::where('user_id', '=', auth()->user()->id)->paginate(20);
        return view('tasks.index', compact('tasks'));
    }

    public function edit(Task $task)
    {
        $clients = Client::all();
        $orders = Order::all();
        return view('tasks.edit', compact('task', 'clients', 'orders'));
    }

    public function create()
    {
        $clients = Client::all();
        $orders = Order::all();
        return view('tasks.create', compact('orders', 'clients'));
    }

    public function update(Request $request, Task $task)
    {
        $request->validate([
            'name' => 'required|string',
            'description' => 'required|string',
            'client' => 'required|integer',
            'order' => 'required|integer'
        ]);

        $task->name = $request->name;
        $task->description = $request->description;
        $task->client_id = $request->client;
        $task->order_id = $request->order;

        if ($task->save()) {
            Session::flash('success', 'Task update');
            return redirect('/tasks');
        }
    }

    public function show(Task $task)
    {
        return view('tasks.show', compact('task'));
    }

    public function destroy(Task $task)
    {
        if (Task::destroy($task->id)) {
            Session::flash('error', 'Task ' . $task->name . ' has been deleted');
            return redirect('/tasks/' . $task->id);
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'description' => 'required|string',
            'duration' => 'required|integer',
            'client' => 'required|integer',
            'order' => 'required|integer',
            'closed' => 'required|integer'
        ]);

        $task = Task::create([
            'name' => $request->name,
            'description' => $request->description,
            'duration' => $request->duration,
            'user_id' => auth()->user()->id,
            'client_id' => $request->client,
            'order_id' => $request->order,
            'approved' => '0',
            'closed' => $request->closed,
        ]);

        if ($task) {
            return redirect('/tasks/' . $task->id);
        }
    }

    public function close(Task $task)
    {

        $task->closed = '1';

        if ($task->save()) {
            $history = History::create([
                'user_id' => auth()->user()->id,
                'task_id' => $task->id,
                'client_id' => $task->client->id,
                'order_id' => $task->order->id,
            ]);
            if ($history) {
                return redirect()->back();
            }
        }
    }
}
