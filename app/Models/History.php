<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{

    protected $fillable = ['user_id', 'task_id', 'client_id', 'order_id'];

    public function clients(){
        return $this->belongsTo(Client::class);
    }

    public function tasks(){
        return $this->belongsTo(Task::class);
    }

    public function users(){
        return $this->belongsTo(User::class);
    }

    public function orders(){
        return $this->belongsTo(Order::class);
    }
}
