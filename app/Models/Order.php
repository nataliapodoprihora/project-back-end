<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $fillable = ['client_id', 'type', 'qty'];

    public function tasks()
    {
        return $this->belongsToMany(Task::class);
    }

    public function client()
    {
        return $this->hasOne(Client::class);
    }

    public function leftTime(Client $client, Order $order)
    {
        $qty = $order->qty;
        $orders = Task::where([['client_id', '=', $client->id]])->get();
        $i = 0;
        foreach ($orders as $time) {
            if ($time->order_id == $order->id) {
                $i = $i + $time->duration;
            }
        }
        return $qty - $i;
    }
}
