<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = ['user_id', 'client_id', 'order_id', 'name', 'description', 'approved', 'closed', 'duration', 'started_at', 'finished_at'];

    public function order(){
        return $this->belongsTo(Order::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function client(){
        return $this->belongsTo(Client::class);
    }

    public function geoPosition(Task $task){
        $coord = [];
        $client_adress = $task->client->adress;
        $url = 'https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($client_adress).'&key=AIzaSyAt1uYLo-hdFL-uYq-fRj4xI8xfZTCC1Uo';
        $resp_url = file_get_contents($url);
        $resp = json_decode($resp_url, true);
        if($resp['status'] == 'OK'){
            $coord['lati'] = isset($resp['results'][0]['geometry']['location']['lat']) ? $resp['results'][0]['geometry']['location']['lat'] : "";
            $coord['longi'] = isset($resp['results'][0]['geometry']['location']['lng']) ? $resp['results'][0]['geometry']['location']['lng'] : "";
        }
        return $coord;
    }
}
