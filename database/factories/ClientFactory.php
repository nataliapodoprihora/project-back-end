<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Client::class, function (Faker $faker) {
    return [
        'user_id'=>$faker->numberBetween(1,10),
        'edrpou'=>$faker->randomNumber(),
        'name' => $faker->name,
        'fullname' => $faker->name,
        'adress' => $faker->streetAddress,
        'legal_adress' => $faker->streetAddress,
    ];
});
