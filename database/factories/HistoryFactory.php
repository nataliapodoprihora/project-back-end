<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\History::class, function (Faker $faker) {
    return [
        'user_id'=>$faker->numberBetween(1,10),
        'task_id'=>$faker->numberBetween(1,10),
        'client_id'=>$faker->numberBetween(1,10),
        'order_id'=>$faker->numberBetween(1,10),
    ];
});
