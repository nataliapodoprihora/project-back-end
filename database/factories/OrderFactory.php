<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Order::class, function (Faker $faker) {
    return [
        'client_id' => $faker->numberBetween(1, 50),
        'type' => $faker->numberBetween(1, 2),
        'qty' => $faker->numberBetween(1, 200),
    ];
});
