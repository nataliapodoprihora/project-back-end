<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Profile::class, function (Faker $faker) {
    return [
        'firstname'=>$faker->firstName,
        'lastname'=>$faker->lastName,
        'country'=>$faker->country,
        'city'=>$faker->city,
        'adress'=>$faker->streetAddress,
        'phone'=>$faker->phoneNumber,
        'sex'=>$faker->randomElement(['male', 'female']),
        'image'=>$faker->imageUrl(),
    ];
});
