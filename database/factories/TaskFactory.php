<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Task::class, function (Faker $faker) {
    return [
        'user_id'=>$faker->numberBetween(1,10),
        'client_id'=>$faker->numberBetween(1,10),
        'order_id'=>$faker->numberBetween(1,10),
        'name'=>$faker->streetName,
        'description'=>$faker->realText(200),
        'duration'=>$faker->numberBetween(1, 8),
        'approved'=>$faker->numberBetween(0,1),
        'closed'=>$faker->numberBetween(0,1),
        'started_at'=>$faker->unixTime,
        'finished_at'=>$faker->unixTime,
    ];
});
