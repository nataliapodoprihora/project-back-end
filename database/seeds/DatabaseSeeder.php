<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\User::class, 10)->create()->each(function ($user) {
            $user->profile()->save(factory(\App\Models\Profile::class)->make());
            factory(\App\Models\Client::class, 10)->create()->each( function ($client) use ($user){
                $client->orders()->saveMany(factory(\App\Models\Order::class, 10)->make());
                $client->orders()->each(function ($task) use ($user, $client){
                    $task = $client->tasks()->saveMany(factory(\App\Models\Task::class, 10)->make(['client_id'=>$client->id]));
//                    $client->tasks()->saveMany(factory(\App\Models\History::class, 10)->create());
                });
            });
            factory(\App\Models\History::class, 10)->create();
        });
    }
}
