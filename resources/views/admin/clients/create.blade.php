@extends('adminlte::page')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1>New client</h1>
                <form method="post" action="/admin/clients" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="edrpou">EDRPOU</label>
                        <input type="text" name="edrpou" id="edrpou" class="form-control" value="" />
                    </div>
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" id="name" class="form-control" value="" />
                    </div>
                    <div class="form-group">
                        <label for="fullname">Name</label>
                        <input type="text" name="fullname" id="fullname" class="form-control" value="" />
                    </div>
                    <div class="form-group">
                        <label for="adress">Adress</label>
                        <input type="text" name="adress" id="adress" class="form-control" value="" />
                    </div>
                    <div class="form-group">
                        <label for="legal_adress">Legal adress</label>
                        <input type="text" name="legal_adress" id="legal_adress" class="form-control" value="" />
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Create client</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
