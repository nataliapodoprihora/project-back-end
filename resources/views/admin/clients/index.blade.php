@extends('adminlte::page')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="pull-right">
                    <a href="/admin/clients/create"  class="btn btn-success">Create client</a>
                </div>
                <h1>Clients</h1>
                <table class="table table-striped table-bordered">
                    <tr>
                        <th>#</th>
                        <th>EDRPOU</th>
                        <th>Registered</th>
                        <th>Name</th>
                        <th>Adress</th>
                        <th></th>
                    </tr>
                    @php $i = 0 @endphp
                    @foreach($clients as $client)
                        @php  $i+=1 @endphp
                        <tr>
                            <td>{{ $i }}</td>
                            <td>{{ $client->edrpou }}</td>
                            <td>{{ $client->created_at }}</td>
                            <td>{{ $client->name }}</td>
                            <td>{{ $client->adress }}</td>
                            <td width="13%">
                                <a href="/admin/clients/{{$client->id}}" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i></a>
                                <a href="/admin/clients/{{$client->id}}/edit" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a>
                                <form method="post" action="/admin/clients/{{$client->id}}" style="display:inline">
                                    @csrf
                                    {{ method_field('DELETE') }}
                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-close"></i> </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </table>
                <div class="pagination">{{$clients->links()}}</div>
            </div>
        </div>
    </div>

@endsection
