@extends('adminlte::page')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1>{{ $client->name }}</h1>
                <div><strong>EDRPOU:</strong> {{ $client->edrpou }}</div>
                <div><strong>Full name:</strong> {{ $client->fullname }}</div>
                <div><strong>Adress:</strong> {{ $client->adress }}</div>
                <div><strong>Legal adress:</strong> {{ $client->legal_adress }}</div>
                <h1>Orders</h1>
                <table class="table table-bordered table-striped">
                    <tr>
                        <th>{{ __('Order id') }}</th>
                        <th>{{ __('Created') }}</th>
                        <th>{{ __('Quantity') }}</th>
                        <th>{{ __('Type') }}</th>
                        <th>{{ __('Left') }}</th>
                    </tr>
                    @foreach($client->orders as $order)
                        <tr>
                            <th>{{ $order->id }}</th>
                            <th>{{ $order->created_at }}</th>
                            <th>{{ $order->qty }}</th>
                            <th>@if($order->type === '1')
                                    Services with access
                                @else
                                    Office services
                                @endif</th>
                            <th>{{$order->leftTime($client, $order)}}</th>
                        </tr>
                    @endforeach
                </table>
            <div class="float-right">
                <a href="/admin/clients/" class="btn btn-success">Back</a>
            </div>
        </div>
    </div>
@endsection
