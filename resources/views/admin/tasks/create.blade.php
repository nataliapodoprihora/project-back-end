@extends('adminlte::page')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1>Create task</h1>
                <form method="post" action="/admin/tasks" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="name">Title</label>
                        <input type="text" name="name" id="name" class="form-control" value=""/>
                    </div>
                    <div class="form-group">
                        <label for="client">Client</label>
                        <select name="client" class="form-control">
                            <option value="" disabled>Select client...</option>
                            @foreach($clients as $client)
                                <option value="{{ $client->id }}">{{ $client->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="order">Order</label>
                        <select name="order" class="form-control">
                            <option value="" disabled>Select order...</option>
                                @foreach($orders as $order)
                                    <option value="{{ $order->id }}">{{ $order->id }}</option>
                                @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea name="description" id="description" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="duration">Duration</label>
                        <input type="number" name="duration" id="duration" class="form-control"></input>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Create task</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
