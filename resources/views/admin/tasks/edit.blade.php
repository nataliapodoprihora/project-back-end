@extends('adminlte::page')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1>Edit {{ $task->name }}</h1>
                <form method="post" action="/admin/tasks/{{ $task->id }}" enctype="multipart/form-data">
                    @csrf
                    {{ method_field('PUT') }}
                    <div class="form-group">
                        <label for="name">Title</label>
                        <input type="text" name="name" id="name" class="form-control" value="{{ $task->name }}" />
                    </div>
                    <div class="form-group">
                        <label for="client">Client</label>
                        <select name="client" class="form-control">
                            <option value="" disabled >Select client...</option>
                            @foreach($clients as $client)
                                <option @if($task->client_id == $client->id) selected @endif value="{{ $client->id }}">{{ $client->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="order">Order</label>
                        <select name="order" class="form-control">
                            <option value="" disabled >Select order...</option>
                            @foreach($orders as $order)
                                <option @if($task->order_id == $order->id) selected @endif value="{{ $order->id }}">{{ $order->id }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="duration">Duration</label>
                        <input name="duration" id="duration" class="form-control" value="{{ $task->duration }}"/>
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea name="description" id="description" class="form-control">{{ $task->description }}</textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Update task</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
