@extends('adminlte::page')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h2>{{__('Tasks')}}</h2>
                <div class="card">
                    <div class="card-body">
                        <div class="pull-right">
                            <a href="/admin/tasks/create" class="btn btn-success">Create task</a>
                        </div>
                        <table class="table table-bordered table-striped">
                            <tr>
                                <th>#</th>
                                <th>{{ __('Client') }}</th>
                                <th>{{ __('Title') }}</th>
                                <th>{{ __('Description') }}</th>
                                <th>{{ __('Duration') }}</th>
                                <td>{{ __('Approved') }}</td>
                                <td>{{ __('Closed') }}</td>
                                <td>{{ __('Active') }}</td>
                            </tr>
                            @php $i = 0 @endphp
                            @foreach($tasks as $task)
                                @php  $i+=1 @endphp
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>@if($task->client) {{ $task->client->name }} @endif</td>
                                    <td>{{ $task->name }}</td>
                                    <td>{{ $task->description }}</td>
                                    <td>{{ $task->duration }}</td>
                                    <td><a href='/admin/tasks/{{$task->id}}/changestatus'>@if($task->approved == 1)<i
                                                class="fa fa-check"></i> @else <i class="fa fa-close"></i>@endif</a>
                                    </td>
                                    <td><a href='/admin/tasks/{{$task->id}}/closed'>@if($task->closed == 1)<i
                                                class="fa fa-check"></i> @else <i class="fa fa-close"></i>@endif</a>
                                    </td>
                                    <td width="13%">
                                        <a href="/admin/tasks/{{$task->id}}" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i></a>
                                        <a href="/admin/tasks/{{$task->id}}/edit" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a>
                                        <form method="post" action="/admin/tasks/{{$task->id}}" style="display:inline">
                                            @csrf
                                            {{ method_field('DELETE') }}
                                            <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-close"></i> </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        <div class="pagination">{{$tasks->links()}}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

