@extends('adminlte::page')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1><strong>{{ $task->name }}</strong></h1>
                <div><strong>Created:</strong> {{ $task->created_at->diffForHumans() }}</div>
                <div><strong>Description:</strong>{{ $task->description }}</div>
                <div><strong>Duration:</strong>{{ $task->duration }}</div>
                <div><strong>Client:</strong>@if($task->client->name){{ $task->client->name}} @endif</div>
                <div><strong>Order:</strong>{{ $task->order->id }}</div>
            </div>
        </div>
        <div class="float-right">
            <a href="/admin/tasks/" class="btn btn-success">Back</a>
        </div>
    </div>
@endsection
