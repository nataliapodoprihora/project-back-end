@extends('adminlte::page')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>Edit user</h1>
                <form method="post" action="/admin/users/{{$user->id}}">
                    @csrf
                    {{ method_field('PUT') }}
                    <div class="form-group">
                        <label class="control-label" for="firstname">Firstname</label>
                        <input type="text" id="firstname" class="form-control"  name="firstname" value="{{$user->profile->firstname}}">
                        @if($errors->has('firstname'))
                            <span class="text-danger">{{$errors->get('firstname')[0]}}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="lastname">Lastname</label>
                        <input type="text" id="lastname" class="form-control"  name="lastname" value="{{$user->profile->lastname}}">
                        @if($errors->has('lastname'))
                            <span class="text-danger">{{$errors->get('lastname')[0]}}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="email">Email</label>
                        <input disabled type="text" id="email" class="form-control"  name="email" value="{{$user->email}}">
                        @if($errors->has('email'))
                            <span class="text-danger">{{$errors->get('email')[0]}}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="password">Password</label>
                        <input type="text" id="password" class="form-control"   name="password" >
                        @if($errors->has('password'))
                            <span class="text-danger">{{$errors->get('password')[0]}}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="password_confirmation">Password confirmation</label>
                        <input type="text" id="password_confirmation" class="form-control"  name="password_confirmation">
                        @if($errors->has('password_confirmation'))
                            <span class="text-danger">{{$errors->get('password_confirmation')[0]}}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="phone">Phone</label>
                        <input type="text" id="phone" class="form-control"  name="phone" value="{{$user->profile->phone}}">
                        @if($errors->has('phone'))
                            <span class="text-danger">{{$errors->get('phone')[0]}}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="country">Country</label>
                        <input type="text" id="country" class="form-control"  name="country" value="{{$user->profile->country}}">
                        @if($errors->has('country'))
                            <span class="text-danger">{{$errors->get('country')[0]}}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="city">City</label>
                        <input type="text" id="city" class="form-control"  name="city" value="{{$user->profile->city}}">
                        @if($errors->has('city'))
                            <span class="text-danger">{{$errors->get('city')[0]}}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="adress">Address</label>
                        <textarea type="text" id="adress" class="form-control"  name="adress">{{$user->profile->adress}}</textarea>
                        @if($errors->has('adress'))
                            <span class="text-danger">{{$errors->get('adress')[0]}}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="sex">Sex</label>
                        <select name="sex" class="form-control">
                            <option value="" selected disabled>Select sex ...</option>
                            <option @if($user->profile->sex == 'male') selected @endif value="male">Male</option>
                            <option @if($user->profile->sex == 'female') selected @endif value="female">Female</option>
                        </select>
                        @if($errors->has('sex'))
                            <span class="text-danger">{{$errors->get('sex')[0]}}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="role">Role</label>
                        <select name="role" class="form-control">
                            <option value="" selected disabled>Select role ...</option>
                            <option @if($user->role == 'user')  selected @endif value="user">User</option>
                            <option @if($user->role == 'admin') selected @endif value="admin">Administrator</option>
                            <option @if($user->role == 'client') selected @endif value="client">Client</option>
                        </select>
                        @if($errors->has('role'))
                            <span class="text-danger">{{$errors->get('role')[0]}}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-lg">Save user</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
