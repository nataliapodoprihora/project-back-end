@extends('adminlte::page')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1>{{$user->profile->firstname." ".$user->profile->lastname}}</h1>
                <div class="panel">
                    <div class="panel-body">
                        <ul class="list-unstyled">
                            <li><strong>Email: </strong>{{$user->email}}</li>
                            <li><strong>Phone: </strong>{{$user->profile->phone}}</li>
                            <li><strong>Sex: </strong>{{$user->profile->sex}}</li>
                            <li><strong>Country: </strong>{{$user->profile->country}}</li>
                            <li><strong>City: </strong>{{$user->profile->city}}</li>
                            <li><strong>Address: </strong>{{$user->profile->adress}}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
