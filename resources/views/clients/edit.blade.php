@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>Edit {{ $client->name }}</h1>
                <form method="post" action="/clients/{{ $client->id }}" enctype="multipart/form-data">
                    @csrf
                    {{ method_field('PUT') }}
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" id="name" class="form-control" value="{{ $client->name  }}" />
                    </div>
                    <div class="form-group">
                        <label for="fullname">Full name</label>
                        <input type="text" name="fullname" id="fullname" class="form-control" value="{{ $client->fullname  }}" />
                    </div>
                    <div class="form-group">
                        <label for="adress">Adress</label>
                        <input type="text" name="adress" id="adress" class="form-control" value="{{ $client->adress  }}" />
                    </div>
                    <div class="form-group">
                        <label for="legaladress">Legal adress</label>
                        <input type="text" name="legaladress" id="legaladress" class="form-control" value="{{ $client->legal_adress  }}" />
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-lg">Update client</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
