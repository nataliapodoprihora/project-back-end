@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-12">
            <h2>{{__('My firms')}}</h2>
            <div class="row">
                <div class="card">
                    <div class="card-body">
                        @auth
                            <div class="float-left">
                                <a href="/clients/create" class="btn btn-success btn-lg">Create new firm</a>
                            </div>
                        @endauth
                        @if(auth()->user())
                            <table class="table table-bordered table-striped">
                                <tr>
                                    <th>{{ __('EDRPOU') }}</th>
                                    <th>{{ __('Name') }}</th>
                                    <th>{{ __('Full name') }}</th>
                                    <th>{{ __('Adress') }}</th>
                                    <th>{{ __('Legal adress') }}</th>
                                    <th>{{ __('Active') }}</th>
                                </tr>
                                @foreach($clients as $client)
                                    <tr>
                                        <th>{{ $client->edrpou }}</th>
                                        <th>{{ $client->name }}</th>
                                        <th>{{ $client->fullname }}</th>
                                        <th>{{ $client->adress }}</th>
                                        <th>{{ $client->legal_adress }}</th>
                                        <th>
                                            @csrf
                                            <a href="/clients/{{$client->id}}" class="btn btn-success btn-sm">
                                                <ion-icon name="eye"></ion-icon>
                                            </a>
                                            <a href="/clients/{{$client->id}}/edit" class="btn btn-warning btn-sm">
                                                <ion-icon name="brush"></ion-icon>
                                            </a>
                                            <form method="post" action="/clients/{{$client->id}}" style="display:inline-block">
                                                @csrf
                                                {{method_field('DELETE')}}
                                                <button type="submit" class="btn btn-danger btn-sm">
                                                    <ion-icon name="trash"></ion-icon>
                                                </button>
                                            </form>
                                        </th>
                                    </tr>
                                @endforeach
                            </table>
                            <div class="pagination">{{$clients->links()}}</div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

