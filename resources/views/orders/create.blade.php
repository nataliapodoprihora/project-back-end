@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>My new order</h1>
                <form method="post" action="/orders" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="type">Services type</label>
                        <select name="type" class="form-control">
                            <option value="" disabled >Select type...</option>
                            <option value="1">Services with access</option>
                            <option value="2">Office services</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="qty">Quantity</label>
                        <input type="text" name="qty" id="qty" class="form-control" value="" />
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-lg">Create order</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
