@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-12">
            <h2>{{__('My orders')}}</h2>
            <div class="row">
                <div class="card">
                    <div class="card-body">
                        @auth
                            <div class="float-right">
                                <a href="/orders/create" class="btn btn-success btn-lg">Create order</a>
                            </div>
                        @endauth
                        @if(auth()->user())
                            <table class="table table-bordered table-striped">
                                <tr>
                                    <th>{{ __('Client') }}</th>
                                    <th>{{ __('Quantity') }}</th>
                                    <th>{{ __('Left time')}}</th>
                                </tr>
                                @foreach($orders as $order)
                                    <tr>
                                        <th>@if($order->client) {{ $order->client->name }} @endif</th>
                                        <th>@if($order->qty) {{ $order->qty }} @endif</th>
                                        <th></th>
                                    </tr>
                                @endforeach
                            </table>
                            <div class="pagination">{{$orders->links()}}</div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

