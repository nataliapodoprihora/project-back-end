@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h1>My profile</h1>
                </div>
                <div class="card-body">
                    <form method="post" action="/profile">
                        @csrf
                        {{ method_field('PUT') }}
                        {{--<div class="form-group">--}}
                            {{--<img src="{{auth()->user()->profile->image}}"> </img>--}}
                            {{--<input type="image" name="image" id="image" class="form-control">--}}
                        {{--</div>--}}
                        <div class="form-group">
                            <label for="firstname">Firstname</label>
                            <input type="text" name="firstname" id="firstname" class="form-control"
                                   value="@if((auth()->user()->profile->firstname)){{(auth()->user()->profile->firstname)}}@endif"/>
                        </div>
                        <div class="form-group">
                            <label for="lastname">Lastname</label>
                            <input type="text" name="lastname" id="lastname" class="form-control"
                                   value="@if((auth()->user()->profile->lastname)){{ auth()->user()->profile->lastname }}@endif"/>
                        </div>
                        <div class="form-group">
                            <label for="country">Country</label>
                            <input type="text" name="country" id="country" class="form-control"
                                   value="@if((auth()->user()->profile->country)){{ auth()->user()->profile->country }}@endif"/>
                        </div>
                        <div class="form-group">
                            <label for="city">City</label>
                            <input type="text" name="city" id="city" class="form-control"
                                   value="@if((auth()->user()->profile->city)){{ auth()->user()->profile->city }}@endif"/>
                        </div>
                        <div class="form-group">
                            <label for="adress">Adress</label>
                            <input type="text" name="adress" id="adress" class="form-control"
                                   value="@if((auth()->user()->profile->adress)){{ auth()->user()->profile->adress }}@endif"/>
                        </div>
                        <div class="form-group">
                            <label for="phone">Phone</label>
                            <input type="text" name="phone" id="phone" class="form-control"
                                   value="@if((auth()->user()->profile->phone)){{ auth()->user()->profile->phone }}@endif"/>
                        </div>
                        <div class="form-group">
                            <label for="sex">Sex</label>
                            <select name="sex" class="form-control">
                                <option value="male" @if(auth()->user()->profile->sex == 'male') selected @endif>Male
                                </option>
                                <option value="female" @if(auth()->user()->profile->sex == 'female') selected @endif>
                                    Female
                                </option>
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btm btn-success btn-lg">Save profile</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
