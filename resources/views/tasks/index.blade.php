@extends('layouts.app')

@section('content')
    <div class="container">
        <br>
        <div class="col-12">
            <h2>{{__('My tasks')}}</h2>
            <div class="row">
                <div class="card">
                    <div class="card-body">
                        @auth
                            <div class="float-right">
                                <a href="/tasks/create" class="btn btn-success btn-lg">Create task</a>
                            </div>
                        @endauth
                        @if(auth()->user())
                            <table class="table table-bordered table-striped">
                                <tr>
                                    <th>{{ __('Client') }}</th>
                                    <th>{{ __('Title') }}</th>
                                    <th>{{ __('Description') }}</th>
                                    <th>{{ __('Duration') }}</th>
                                    <th>{{ __('Active') }}</th>
                                    <th>{{ __('Status') }}</th>
                                </tr>
                                @foreach($tasks as $task)
                                    <tr>
                                        <td>@if($task->client) {{ $task->client->name }} @endif</td>
                                        <td>{{ $task->name }}</td>
                                        <td>{{ $task->description }}</td>
                                        <td>{{ $task->duration }}</td>
                                        <td>
                                            @csrf
                                            <a hidden="true">{{$coord = json_encode($task->geoPosition($task))}}</a>
                                            <geoposition :coord="{{$coord}}" :id="{{ $task->id }}"></geoposition>
                                            <a href="/tasks/{{$task->id}}/edit" class="btn btn-warning btn-sm">
                                                <ion-icon name="brush"></ion-icon>
                                            </a>
                                            <form method="post" action="/tasks/{{$task->id}}"
                                                  style="display:inline-block">
                                                @csrf
                                                {{method_field('DELETE')}}
                                                <button type="submit" class="btn btn-danger btn-sm">
                                                    <ion-icon name="trash"></ion-icon>
                                                </button>
                                            </form>
                                        </td>
                                        <td>
                                            @csrf
                                            <a href="/tasks/{{$task->id}}/close" class="btn btn-success btn-sm">
                                                <ion-icon name="checkmark"></ion-icon>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                            <div class="pagination">{{$tasks->links()}}</div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

