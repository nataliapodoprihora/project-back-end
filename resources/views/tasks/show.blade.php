@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1><strong>{{ $task->name }}</strong></h1>
                <div><strong>Created:</strong> {{ $task->created_at->diffForHumans() }}</div>
                <div><strong>Description:</strong>{{ $task->description }}</div>
            </div>
        </div>
        <div class="float-right">
            <a href="/tasks/" class="btn btn-success btn-lg">Back</a>
        </div>
    </div>
@endsection
