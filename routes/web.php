<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
/**/
Route::get('/', 'HomeController@index')->name('home');
Route::get('login', 'AuthController@showLogin')->name('login');
Route::post('login', 'AuthController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('register', 'AuthController@showLogin')->name('register');
Route::post('register', 'AuthController@register');

Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/profile', 'ProfileController@index');
Route::put('/profile', 'ProfileController@update');

Route::group(['prefix' => 'tasks', 'middleware' => 'role_user'], function () {
    Route::get('/', 'TaskController@index');
    Route::get('/create','TaskController@create');
    Route::get('/{task}','TaskController@show');

    Route::get('/{task}/edit','TaskController@edit');
    Route::get('/{task}/close','TaskController@close');
    Route::put('/{task}','TaskController@update');
    Route::delete('/{task}','TaskController@destroy');

    Route::post('/','TaskController@store');
    Route::get('/{task}/geo', 'TaskController@geoPosition');
});

Route::group(['prefix' => 'orders', 'middleware' => 'role_client'], function () {
    Route::get('/', 'OrderController@index');
    Route::get('/create','OrderController@create');
    Route::get('/{order}','OrderController@show');

    Route::get('/{order}/edit','OrderController@edit');
    Route::put('/{order}','OrderController@update');
    Route::delete('/{order}','OrderController@destroy');

    Route::post('/','OrderController@store');
});

Route::group(['prefix' => 'clients', 'middleware' => 'role_client'], function () {
    Route::get('/', 'ClientController@index');
    Route::get('/create','ClientController@create');
    Route::get('/{client}','ClientController@show');

    Route::get('/{client}/edit','ClientController@edit');
    Route::put('/{client}','ClientController@update');
    Route::delete('/{client}','ClientController@destroy');

    Route::post('/','ClientController@store');
});

//admin routes
Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'role_admin'], function () {
    Route::resource('tasks', 'TaskController');
    Route::get('tasks/{task}/changestatus', 'TaskController@changePublishedStatus');
    Route::get('tasks/{task}/closed', 'TaskController@changeClosed');
    Route::resource('users', 'UserController');
    Route::resource('invites', 'InviteController');
    Route::resource('clients', 'ClientController');
});

